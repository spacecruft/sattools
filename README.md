# Libre Sattools
Libre Sattools is a free software fork of parts of Cees Bassa's `sattools`.

Current state: `skymap`, `satmap`, and `satorbit` have been ported to
libre giza. Additional features have been added to `skymap`.

![skymap-giza rev 21](img/skymap-giza-21.png)

See above screenshot from port of `skymap`.

![satmap-giza rev 3](img/satmap-giza-3.png)

See above screenshot from port of `satmap`.

![satorbit-giza rev 3](img/satorbit-giza-3.png)

See above screenshot from port of `satorbit`.

![satid-giza rev 3](img/satid-giza-3.png)

See above screenshot from port of `satid`.

![stvid-example](examples/stvid-sample.png)
PNG of FITS image from `stvid`, used in example below.

![satid-example](examples/satid-sample.png)

Above, screenshot of this command:

```
satid examples/satid-sample.fits
mv satid.png examples/satid-sample.png
```

Note: R.A. and Decl. issue.
Values are read from FITS (?).


# Features
The `giza` branch is mostly similar to upstream, but with
`giza` instead of `pgplot`.

The following (mis?)-features have been added to `skymap` in the active
`spacecruft` branch:

* Optionally resize window on command line with `--width` and `--height`.
  (Probably best to just get width and calculate a good height.)

* Optionally change resolution of drawing lines with `--nmax`.
  (Probably best to calculate nmax depending on window size.)

* Add SatNOGS (now default), last 30 days, Space Stations, Active Satellites,
  GNSS, and visual TLEs.

* Update SGP4 files to latest upstream.

* Add long options (e.g. `--help`) on the command line.

* A local version of bits of qfits is included, so no
  upstream install is required to build.
  (Are the binary apps needed elsewhere?)


# Build
Development is done in Debian Bookworm (testing).
Tested and working with Debian Bullseye (11/stable)
Doesn't require any dependencies outside of Debian.

```
sudo apt install giza-dev git make dos2unix source-extractor wcslib-dev \
     libgsl-dev gfortran libpng-dev libx11-dev libjpeg-dev libexif-dev

git clone https://spacecruft.org/spacecruft/sattools

cd sattools/

make

sudo make install

# To clean
make clean

# To rebuild
make clean
make

# To uninstall
sudo make uninstall
```

Built files get put in the `bin/` directory.


# Usage
```
cd bin/
./tleupdate

# set config in ~/.bashrc XXX

./skymap --help
Usage: skymap [OPTION]
Visualize satellites on a map of the sky.

-t, --time    Date/time (yyyy-mm-ddThh:mm:ss.sss) [default: now]
-c, --catalog    TLE catalog file [default: satnogs.tle]
-i, --id    Satellite ID (NORAD) [default: all]
-R, --ra    R.A. [hh:mm:ss.sss]
-D, --decl    Decl. [+dd:mm:ss.ss]
-A, --azimuth    Azimuth (deg)
-E, --elevation    Elevation (deg)
-w, --width    Screen width (default: 1024). Set height too.
-g, --height    Screen height (default: 768). Set width too.
-n, --nmax    nmax line resolution/speed (default 128)
-S, --all-night    All night
-Q, --no-stars    No stars
-a, --all-objects    Show all objects from catalog (default: LEO)
-h, --help    This help
-s, --site    Site (COSPAR)
-d, --iod    IOD observations
-l, --length    Trail length [default: 60s]
-P, --planar-id    planar search satellite ID
-r, --planar-alt    planar search altitude
-V, --visibility-alt    altitude for visibility contours
-p, --positions-file    File with xyz positions
-L, --longitude    manual site longitude (deg)
-B, --latitude    manual site latitude (deg)
-H, --elevation    manual site elevation (m)
```


# Port
Info about the port to free software `giza` instead of
proprietary and unmaintained `pgplot` can be seen in the `PORT.md` file.


# Upstream
See `README-upstream.md`.

Sattools, GPLv3.

* https://github.com/cbassa/sattools

Upstream has ported many of the tools to Python.
See also:

* https://github.com/cbassa/strf

* https://github.com/cbassa/stvid

* https://github.com/cbassa/asm

* https://github.com/cbassa/stphot


Giza, GPLv2.

* https://danieljprice.github.io/giza

SGP4, LGPLv3:

* https://gitlab.com/pscrawford/dundee_sgp4/

qfits, libre permissive:

* ftp://ftp.eso.org/pub/qfits


# License
Same as upstream `sattools`, GPLv3.

*Copyright (C) 2022, Jeff Moe*

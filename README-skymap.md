# skymap Mini HOWTO
HOWTO find a particular satellite for viewing with skymap
for use with stvid imaging.

From notes by Cees Bassa mixed with my misunderstandings.
This example will use the Bluewalker 3 satellite, with NORAD ID 53807.


# TLE
First a TLE is needed. I get this from Celestrak, ala:

```
wget -O ~/devel/spacecruft/TLE/bluewalker3/bluewalker3.tle \
  "https://celestrak.org/NORAD/elements/gp.php?CATNR=53807"
```

Should look something like this:

```
BLUEWALKER 3            
1 53807U 22111AL  22323.16311390  .00001894  00000+0  10565-3 0  9997
2 53807  53.2016 312.3504 0014195 129.0685 231.1577 15.18596984 10510
```

# Skymap Script
Run skymap (with my branch):

```
#!/bin/bash

LENGTH=86400
TIME="2022-11-19T17:20:45.000"
TLE=~/devel/spacecruft/TLE/bluewalker3/bluewalker3.tle
SITE=9990
LAT="40.568912"
LONG="-105.225852"
# Set elevation in data/sites.txt
# 9990 JM  40.568912  -105.225852     1843    Jeff Moe
#ELEVATION="1843"

skymap									\
	--no-stars							\
	--width=1600							\
	--height=1000							\
	--all-objects							\
	--nmax=128							\
	--length=${LENGTH}						\
	--catalog=${TLE}						\
	--latitude="${LAT}"						\
	--longitude="${LONG}"						\
	--time="${TIME}"
```

# Skymap Window Overview
![skymap tutorial main](img/skymap-tutorial-main.png)

Above is a 24 hour (86400 seconds) prediction for
Bluewalker 3 for the example location.

* Grey tracks are in the shadow.
* Orange tracks are in sunlight but during daytime.
* Yellow tracks are in sunlight but during night time.

The example has two passes during night time:

* Low South East.
* Low South West.

![skymap tutorial south east](img/skymap-tutorial-south-east.png)
![skymap tutorial south west](img/skymap-tutorial-south-west.png)


# Skymap Interactive

In interactive mode, use `l` to change the track length to 3600 secs (1 hour).
To use `skymap` in interactive mode, launch `skymap` from the command line.
When you hit `l` in the graphical display, the prompt will be in the terminal
window.

Then use `.` to move time forward with these 1 hour steps.
It makes it faster if `skymap` is run without drawing stars until needed,
especially during frequent re-renders.
Disable viewing stars with `shift-Q`.

![skymap tutorial timestep](img/skymap-tutorial-timestep.png)

At some point you'll get the first pass.
* Use `<` a few times to reduce the timestep size to something like 450 seconds.
* Step forward with `.` until it is above the horizon.
* In example, move time forward to `00:37:37`.

![skymap tutorial 00:37:37](img/skymap-tutorial-3737.png)


# Skymap Satellite Info
Press `i` with the cursor near `53807`.
It gives this output:

```
BLUEWALKER 3            
1 53807U 22111AL  22323.16311390  .00001894  00000+0  10565-3 0  9997
2 53807  53.2016 312.3504 0014195 129.0685 231.1577 15.18596984 10510

Age: 0.9 d

x:   +5351.2801 km; vx: -0.38650 km/s
y:   -2422.0234 km; vy: +6.04035 km/s
z:   +3590.7817 km; vz: +4.61568 km/s
r:    1633.97 km; v:  -0.343 km/s
l: -92.78; b:  31.44; h:     506.35 km

R.A.:  01:13:17.38  Decl.: -19:10:17.4
R.A.:  01:12:10.33  Decl.: -19:17:33.2 (J2000)
Azi.: 127.8 Alt.: 11.3

Phase: 53.17
Magnitude: 6.25
Angular velocity: 0.2593 (deg/s)
```

This gives you the Az/El and RA/Dec to read off.


# TLE with `tleinfo`
Cees dumped the TLE for `53807` into a text file with

```
tleinfo -i 53807 >53807.txt
```

This results in two TLEs, though ? XXX compare with TLE from above.

```
BLUEWALKER-3            
1 53807U 22111AL  22291.57105995  .00000747  00000+0  51211-4 0  9998
2 53807  53.2021  96.3521 0013619  43.3355 316.8709 15.18466480  5717
0 BLUEWALKER 3
1 53807U 22111AL  22291.57105995  .00000747  00000-0  51211-4 0  9999
2 53807  53.2021  96.3521 0013619  43.3355 316.8709 15.18466480  5717
```

# Views
View with `e` for East.

![skymap East view](img/skymap-tutorial-east-view.png)

View with `s` for South.

![skymap South view](img/skymap-tutorial-south-view.png)


# gpredict
This is view of BW3 in `gpredict`, for comparison.

![skymap tutorial gpredict](img/skymap-tutorial-gpredict-bw3.png)


# stellarium
This is view of BW3 in `stellarium`, for comparison.

![skymap tutorial stellarium](img/skymap-tutorial-stellarium-bw3.png)


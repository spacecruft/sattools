# Makefile

prefix = /usr/local
bindir = $(prefix)/bin

all:
	cd qfits ; ./configure ; mkdir -p lib; make 
	$(MAKE) -C src

clean:
	rm -f *.o
	rm -f *~
	rm -f src/*.o
	rm -f src/*~
	rm -fr bin/
	rm -f qfits/config.make
	rm -f qfits/src/*.o
	rm -rf qfits/lib

install:
	@cp -vp bin/*  $(bindir)/

uninstall:
	@rm -vf \
	 $(bindir)/addwcs \
	 $(bindir)/allnight \
	 $(bindir)/angular \
	 $(bindir)/average.jpg \
	 $(bindir)/calibrate \
	 $(bindir)/confirm \
	 $(bindir)/csv2tle \
	 $(bindir)/dec2s \
	 $(bindir)/deproject \
	 $(bindir)/detect \
	 $(bindir)/fakeiod \
	 $(bindir)/faketle \
	 $(bindir)/fitsheader \
	 $(bindir)/fitskey \
	 $(bindir)/imgstat \
	 $(bindir)/jpg2fits \
	 $(bindir)/jpgstack \
	 $(bindir)/launchtle \
	 $(bindir)/measure \
	 $(bindir)/mvtle \
	 $(bindir)/normal \
	 $(bindir)/pass \
	 $(bindir)/pgm2fits \
	 $(bindir)/plotfits \
	 $(bindir)/posmatch \
	 $(bindir)/posvel \
	 $(bindir)/propagate \
	 $(bindir)/pstrack \
	 $(bindir)/rde2iod \
	 $(bindir)/reduce \
	 $(bindir)/residuals \
	 $(bindir)/runsched \
	 $(bindir)/s2dec \
	 $(bindir)/satfit \
	 $(bindir)/satid \
	 $(bindir)/satmap \
	 $(bindir)/satorbit \
	 $(bindir)/skymap \
	 $(bindir)/slewto \
	 $(bindir)/stviewer \
	 $(bindir)/tle2ole \
	 $(bindir)/tleinfo \
	 $(bindir)/tleupdate \
	 $(bindir)/uk2iod \
	 $(bindir)/waitfor \
	 $(bindir)/wcsfit \
	 $(bindir)/xyz2tle


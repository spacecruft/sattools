# Sattools PGPlot Port
This is a fork of Cees Bassa's `sattools` to get it to work with
free software `giza` instead of proprietary and unmaintained `pgplot`.


# Giza
Here are two examples of how `skymap` should render, in this case using
`pgplot` (thx Pierros and Cees):

![skymap-pgplot-pierros](img/skymap-pgplot-pierros.png)

![skymap-pgplot-cbassa](img/skymap-pgplot-cbassa.png)

This is how `skymap` renders "out-of-the-box" using `giza` as a
drop-in replacement for `pgplot`:

![skymap-giza rev 0](img/skymap-giza-0.png)

Stars fix:

![skymap-giza rev 1](img/skymap-giza-1.png)

Text fix:

![skymap-giza rev 2](img/skymap-giza-2.png)

Rectangle fix, and grey:

![skymap-giza rev 4](img/skymap-giza-4.png)

More `pgplot` colors.

![skymap-giza rev 5](img/skymap-giza-5.png)

Giza sizing needs fixing...

![skymap-giza rev 6](img/skymap-giza-6.png)

SatNOGS TLE loads and renders.

![skymap-giza rev 7](img/skymap-giza-7.png)

There is an error that spams out to the console:

```
%giza - Warning - giza_set_character_height: Invalid character height, character height not set
```

The character heights are set to various values, typically `0.6`, `0.8`, and
`1.0`. Only `1.0` doesn't produce the error. But it renders much larger...
This has no errors, but looks bad. Overall need new font.

![skymap-giza rev 8](img/skymap-giza-8.png)

Satellite orbit lines rendering.

![skymap-giza rev 9](img/skymap-giza-9.png)

More, but not rendering so great...

![skymap-giza rev 11](img/skymap-giza-11.png)

Grid, but rendering with dots, not lines...

![skymap-giza rev 12](img/skymap-giza-12.png)

Rest of grid (blue background during day).

![skymap-giza rev 13](img/skymap-giza-13.png)

Equatorial. Hit `o` when `skymap` first launches, then it will re-render
with a faint equatorial "dot line circle"...

![skymap-giza rev 14](img/skymap-giza-14.png)

Remove broken constellations looks better for now.

![skymap-giza rev 15](img/skymap-giza-15.png)

fix orbit lines, maybe correct. need to confirm against main version.

![skymap-giza rev 16](img/skymap-giza-16.png)

Revert back to original size. Full size now, see top of page.

![skymap-giza rev 17](img/skymap-giza-17.png)

More:

![skymap-giza rev 18](img/skymap-giza-18.png)

![skymap-giza rev 19](img/skymap-giza-19.png)

![skymap-giza rev 20](img/skymap-giza-20.png)


# Works
Testing/dev in Debian Bookworm (testing), with `giza` version `1.3.2`.

Out of box, this works:
* Sun.
* Moon.
* Besides the display, all the other functionality works.
  Can load a `satnogs.tle`, get satellite info, schedule, etc.


# Fixed
`skymap`, `satmap`, and `satorbit` have been ported.

* Stars.
* Text header with date, observer, lat/lon/elevation.
* Footer with R, D etc.
* Rectangle.
* Apex renders.
* Grid renders.
* Satellites' colors.
* Orbit lines.
* FOV (but disabled by default upstream).
* Lines.
* Small window size with giza in `satorbit` and `satmap`.
* Constellations.
* Some speed ups.
* Font, sans serif, placement.


# TODO
* Slow.
* Graves.


# Functions
These are `pgplot` functions used by `skymap`.

```
cpgbox
cpgcirc
cpgconl
cpgcont
cpgcurs
cpgdraw
cpgend
cpgeras
cpgmove
cpgmtxt
cpgopen
cpgpap
cpgpt1
cpgqch
cpgqci
cpgqls
cpgrect
cpgsch
cpgsci
cpgscr
cpgsfs
cpgsls
cpgslw
cpgsvp
cpgtext
cpgwnad
```


# Color
Colors 1-15 are used in the color index, documented here:

* https://sites.astro.caltech.edu/~tjp/pgplot/fig51.html

Colors are off because giza uses it's own palette by default.
What is needed is to add `giza.h` and set this to use pgplot colors:

```
giza_set_colour_palette(1);
```


# Satorbit
`satorbit` has the same issues as `skymap`.

Here is what it looks like out-of-the-box with Giza:

![satorbit-giza rev 1](img/satorbit-giza-1.png)

Fixes to colors, and "lines" drawn as dots:

![satorbit-giza rev 2](img/satorbit-giza-2.png)

Latest at top of page.


# Satmap
`satmap` has the same issues as `skymap`.

Here is what it looks like out-of-the-box with Giza:

![satmap-giza rev 1](img/satmap-giza-1.png)

Fixes for color, screensize, and dots for lines...

![satmap-giza rev 2](img/satmap-giza-2.png)


# Satid

`satid` has the same issues as `skymap`.

Example `satid` `2022-08-09T20:39:54.823.fits.png` using `pgplot`:

![satid-pgplot-cbassa](img/satid-pgplot-cbassa.png)

Here is what it looks like out-of-the-box with Giza:

![satid-giza rev 1](img/satid-giza-1.png)

Fixes for color, screensize, and dots for lines...

![satid-giza rev 2](img/satid-giza-2.png)

Background and text colors.

![satid-giza rev 3](img/satid-giza-3.png)


# License
GPLv3.

*Copyright (C) 2022, Jeff Moe*

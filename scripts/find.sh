#!/bin/bash

# Copy first file
if [ ! -e "test.fits" ]; then
    ls -1 2*.fits | head -n1 | awk '{printf("cp %s test.fits\n",$1)}' | sh
fi

# Loop over files
for file in *.fits; do

    if [ ! -e $file.cat ]; then
    # Run sextractor
	sextractor $file -c $ST_DATADIR/sextractor/nondefault.sex 2>/dev/null
	mv test.cat $file.cat
    fi
done

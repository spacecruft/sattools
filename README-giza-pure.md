# Giza Pure
This branch is a "pure" version of giza, without using any PGPlot.


# Status
The pgplot library has been removed. Nearly everything works.

To fix:

* `cpgconl` and `cpgcont` with `giza_contour_float` for
  visibility contours.


# Giza Upstream
Giza is a libre replacement for PGPlot, available in Debian
(`libgiza0` and `giza-dev`).

Main website:
* https://danieljprice.github.io/giza/

Source code:
* https://github.com/danieljprice/giza.git

API:
* https://danieljprice.github.io/giza/documentation/api.html

PGPlot interface. This is being removed from `sattools`.
The giza/pgplot interface packages in Debian are
`libcpgplot0` and `libpgplot0`.
* https://danieljprice.github.io/giza/documentation/pgplot.html

Giza uses Cairo:
* https://www.cairographics.org/

